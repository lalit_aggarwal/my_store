﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Configuration_VHDL
{
    public partial class Form1 : Form
    {
        Configuration_Declaration cnfg_dec;
        Configuration_Specification cnfg_spec;
        public Form1()
        {
            InitializeComponent();
        }

        private void declarationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            cnfg_dec.Show();
        }

        private void specificationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            cnfg_spec.Show();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            cnfg_dec = new Configuration_Declaration();
            cnfg_spec= new Configuration_Specification();
        }
    }
}
