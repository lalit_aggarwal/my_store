﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Configuration_VHDL
{
    public partial class Configuration_Specification : Form
    {
        Form1 frm1 = new Form1();
        string gen = String.Empty;
        string port = String.Empty;
        public Config_spec obj;

        public Configuration_Specification()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            obj = new Config_spec(textBox1.Text, textBox2.Text, textBox3.Text, port, gen);
            this.Hide();
            frm1.richTextBox1.Text += obj.ToString() + Environment.NewLine;
            frm1.Show();
            reset();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            reset();
        }

        void reset()
        {
            textBox1.Text = String.Empty;
            textBox2.Text = String.Empty;
            textBox3.Text = String.Empty;
            textBox4.Text = String.Empty;
            comboBox1.Text = String.Empty;
            comboBox2.Text = String.Empty;
            comboBox3.Text = String.Empty;
            comboBox4.Text = String.Empty;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Hide();
            frm1.Show();
        }

        private void Configuration_Specification_Load(object sender, EventArgs e)
        {
            obj = new Config_spec();
        }
        int count_gen = 0;
        private void button6_Click(object sender, EventArgs e)
        {
            if (textBox4.Text != String.Empty)
                gen = textBox4.Text;
            else
            {
                if (count_gen == 0)
                    gen += comboBox3.Text + "=>" + comboBox4.Text;
                else
                    gen += " , " + comboBox3.Text + "=>" + comboBox4.Text;
            }
            count_gen++;
        }

        private void button7_Click(object sender, EventArgs e)
        {
            gen = String.Empty;
        }
        int count_port = 0;
        private void button4_Click(object sender, EventArgs e)
        {
            if (count_port == 0)
                port += comboBox1.Text + "=>" + comboBox2.Text;
            else
                port += " , " + comboBox1.Text + "=>" + comboBox2.Text;
            count_port++;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            port = String.Empty;
        }
    }
}
