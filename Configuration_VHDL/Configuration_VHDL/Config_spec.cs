﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Configuration_VHDL
{
    public class Config_spec
    {
        public string for_items;
        public string component_name;
        public string entity_name;
        public string port_map;
        public string generic_map;

        public Config_spec()
        {
            for_items = String.Empty;
            component_name = String.Empty;
            entity_name = String.Empty;
            port_map = String.Empty;
            generic_map = String.Empty;
        }

        public Config_spec(string for_items, string component_name, string entity_name,string port_map, string generic_map)
        {
            this.for_items = for_items;
            this.component_name = component_name;
            this.entity_name = entity_name;
            this.port_map = port_map;
            this.generic_map = generic_map;
        }

        public override string ToString()
        {
            string str = " ";
            str = "for " + for_items + " : " + component_name + Environment.NewLine;
            str += "use entity " + entity_name + Environment.NewLine;
            if (generic_map != String.Empty)
                str += "generic map (" + generic_map + ")" + Environment.NewLine;
            if (port_map != String.Empty)
                str += "port map (" + port_map + ")" + Environment.NewLine;
            return str;
        }
    }
}
