﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Sudoku
{
    public partial class Form1 : Form
    {
        TextBox[] tb = new TextBox[81];
        int boxes = 81;
        public Form1()
        {
            InitializeComponent();
        }
        int i,j,k;
        private void Form1_Load(object sender, EventArgs e)
        {
            for (i = 0; i < boxes ; i++)
            {
                tb[i] = new TextBox();
                tb[i].Name = "TextBoxName" + (i+1).ToString();
                tb[i].Size = new Size(35, 20);
                tb[i].TabIndex = i + 1;
                tb[i].Text = String.Empty;
                tb[i].TextAlign = HorizontalAlignment.Center;
                flowLayoutPanel1.Controls.Add(tb[i]);
            }
        }

        int[,] a = new int[9,9];
        int un = 0, tmp;
        bool b = true;
        bool tmp_b = true;
        private void button1_Click(object sender, EventArgs e)
        {
            i = j = 0;
            for (k = 0; k < boxes; k++)
            {
                if (String.Equals(tb[k].Text, String.Empty))
                {
                    MessageBox.Show("INSERT THE VALUES FIRST");
                    break;
                }
                else
                {
                    a[i, j] = int.Parse(tb[k].Text);
                    if (a[i, j] == 0)
                        un++;
                    else
                        tb[k].BackColor = Color.Red;
                    j++;
                    if (j == 9)
                    {
                        i++;
                        j = 0;
                    }
                }
            }
            if (un > 0)
                tmp_b = true;
            while (tmp_b && b)
            {
                b = false;
                for (i = 0; i < 9; i++)
                {
                    for (j = 0; j < 9; j++)
                    {
                        if (a[i,j] != 0)
                            continue;
                        tmp = 0;
                        for (int x = 1; x < 10; x++)
                        {
                            if (row(i, j, x) && column(i, j, x) && square(i, j, x))
                            {
                                if (tmp == 0)
                                    tmp = x;
                                else
                                {
                                    tmp = 0;
                                    break;
                                }
                            }
                        }
                        if (tmp != 0)
                        {
                            a[i,j] = tmp;
                            b = true;
                            un--;
                            if (un > 0)
                                tmp_b = true;
                        }
                    }
                }
            }
            if (b)
                MessageBox.Show("THIS SUDOKU CAN'T BE SOLVED");
            else
            {
                k = 0;
                for (i = 0; i < 9; i++)
                {
                    for (j = 0; j < 9; j++)
                    {
                        tb[k++].Text = a[i, j].ToString();
                    }
                }
            }
        }

        bool row(int s, int o, int n)
        {
            for (int g = 0; g < 9; g++)
            {
                if (a[s,g] == n)
                {
                    return false;
                }
            }
            return true;
        }

        bool column(int s, int o, int n)
        {
            for (int g = 0; g < 9; g++)
            {
                if (a[g,o] == n)
                    return false;
            }
            return true;
        }

        bool square(int s, int o, int n)
        {
            int g, h;
            double row=Math.Ceiling((s+1)/3.0);
            double column=Math.Ceiling((o+1)/3.0);
            int tmp_x = Convert.ToInt32((row-1)*3);
            int tmp_y = Convert.ToInt32((column-1)*3);
            for(g=tmp_x;g<(tmp_x+3);g++)
                for(h=tmp_y;h<(tmp_y+3);h++)
                {
                    if(a[g,h]==n)
                        return false;
                }
            return true;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            for (k = 0; k < boxes; k++)
            {
                tb[k].Text = String.Empty;
                tb[k].BackColor = Color.White;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}




